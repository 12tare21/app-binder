## What is Binder

This library provides you app binding for you and makes your AppServiceProvider cleaner where only Binder service object needs to be instantied and call its methods called in register and boot, it will automatically load interfaces and implementations you provide in config/binds.php and bind them together.

## Installation

In your **composer.json** add fields

    ....
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/12tare21/app-binder"
        },
        .....
    ],
    ....
    "require": {
        ....
        "tariche/binder": "master"
        ....
    },

In your **config/app.php** add next line in your providers array:

    tariche\binder\BinderServiceProvider::class,


and run **composer update** and **vendor:publish** to publish binds.php config file.


