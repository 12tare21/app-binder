<?php

namespace tariche\binder;

class Binder {
    protected $bindings;

    public function __construct(){
        $this->bindings = config('binds') ?? [];
    }

    public function register($app){
        foreach($this->bindings['register'] as $bindObject){
            $this->resolveBindObject($app, $bindObject);
        }
    }

    public function boot($app){
        foreach($this->bindings['boot'] as $bindObject){
            $this->resolveBindObject($app, $bindObject);
        }
    }

    private function resolveBindObject($app, $bindObject){
        try{
            if($bindObject instanceof BindInterfaceTemplate){
                $app->bind($bindObject->interface, $bindObject->implementation);
            } else if($bindObject instanceof ContextualBindTemplate){
                $app->when($bindObject->when)->needs($bindObject->needs)->give($bindObject->give);
            } 
        }
        catch(\Exception $e){
            throw new \Exception('Paths of classes/interfaces provided in binds.php config file are invalid and cannot be binded');
        }
    }
}