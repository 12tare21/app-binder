<?php

use tariche\binder\BindFactory;

return [
    /*
    array of instances of bind objects with interfaces and implementations as parameters to be binded with two static methods
    e.g.: BindFactory::createInterfaceTemplate('path_to_interface', 'path_to_implementation')

    or contextual binding
    e.g.: BindFactory::createContextualTemplate('path_to_when', 'path_to_needs', 'give')    
    */ 

    'register' => [

    ],

    'boot' => [

    ]
];
