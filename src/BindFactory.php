<?php

namespace tariche\binder;

class BindInterfaceTemplate{
    public $interface;
    public $implementation;

    public function __construct($interface, $implementation)
    {
        $this->interface = $interface;
        $this->implementation = $implementation;
    }
}

class ContextualBindTemplate{
    public $when;
    public $needs;
    public $give;

    public function __construct($when, $needs, $give)
    {
        $this->when = $when;
        $this->needs = $needs;
        $this->give = $give;
    }
}

class BindFactory{
    public static function createInterfaceTemplate($interface, $implementation){
        return new BindInterfaceTemplate($interface, $implementation);
    }

    public static function createContextualTemplate($when, $needs, $give){
        return new ContextualBindTemplate($when, $needs, $give);
    }
}