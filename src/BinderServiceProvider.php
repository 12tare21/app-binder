<?php

namespace tariche\binder;

use Illuminate\Support\ServiceProvider;

class BinderServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([__DIR__ . '/binds.php' => config_path('binds.php')]);
    }
}
